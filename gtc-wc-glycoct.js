import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class GtcWcGlycoCT extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_glycoct?accNum={{accession}}" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <template is="dom-repeat" items="{{sampleids}}">
      <pre>{{item.GlycoCT}}</pre>
    </template>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      accession: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('gtc-wc-glycoct', GtcWcGlycoCT);
