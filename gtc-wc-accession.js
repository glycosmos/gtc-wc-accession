import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class GtcWcAccession extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */
a {
  text-decoration: none;
}

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs2GTCID?wurcs={{wurcs}}" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <template is="dom-repeat" items="{{sampleids}}">
      <p><a href="https://glytoucan.org/Structures/Glycans/{{item.id}}" target="_blank">{{item.id}}</a></p>
    </template>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      wurcs: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('gtc-wc-accession', GtcWcAccession);
